# Welcome to the Tech Triage FAQ page

This is a site created by Webster Groves parents for supporting technical issues. 
FAQs and solutions will be published as learned. 

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
